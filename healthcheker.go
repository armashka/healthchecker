package healthchecker

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"sync"
	"time"
)

const (
	up   = "UP"
	down = "DOWN"
)

// builder pattern later

type response struct {
	Status  string            `json:"status,omitempty"`
	Errors  map[string]string `json:"errors,omitempty"`
	Details map[string]*items `json:"details,omitempty"`
}

type items struct {
	Status  string                 `json:"status,omitempty"`
	Details map[string]interface{} `json:"details,omitempty"`
}

type health struct {
	checkers map[string]Checker
	details  map[string]*items
	timeout  time.Duration
}

// Checker checks the status of the dependency and returns error.
// In case the dependency is working as expected, return nil.
type Checker interface {
	Check(ctx context.Context) error
}

// CheckerFunc is a convenience type to create functions that implement the Checker interface.
type CheckerFunc func(ctx context.Context) error

// Check Implements the Checker interface to allow for any func() error method
// to be passed as a Checker
func (c CheckerFunc) Check(ctx context.Context) error {
	return c(ctx)
}

// Handler returns an http.Handler
func Handler(opts ...Option) http.Handler {
	h := &health{
		checkers: make(map[string]Checker),
		details:  make(map[string]*items),
		timeout:  30 * time.Second,
	}
	for _, opt := range opts {
		opt(h)
	}
	return h
}

func WithDetails(name string, details map[string]interface{}) Option {
	return func(h *health) {
		h.details[name] = &items{
			Status:  up,
			Details: details,
		}
	}
}

// HandlerFunc returns a http.HandlerFunc to mount the API implementation at a specific route
func HandlerFunc(opts ...Option) http.HandlerFunc {
	return Handler(opts...).ServeHTTP
}

// Option adds optional parameter for the HealthcheckHandlerFunc
type Option func(*health)

func WithChecker(name string, s Checker) Option {
	return func(h *health) {
		h.checkers[name] = &timeoutChecker{s}
	}
}

func (h *health) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	nCheckers := len(h.checkers)

	code := http.StatusOK
	errorMsgs := make(map[string]string, nCheckers)

	ctx, cancel := context.Background(), func() {}
	if h.timeout > 0 {
		ctx, cancel = context.WithTimeout(ctx, h.timeout)
	}
	defer cancel()

	var mutex sync.Mutex
	var wg sync.WaitGroup
	wg.Add(nCheckers)

	for key, checker := range h.checkers {
		go func(key string, checker Checker) {
			if err := checker.Check(ctx); err != nil {
				mutex.Lock()
				errorMsgs[key] = err.Error()
				code = http.StatusServiceUnavailable

				if item, ok := h.details[key]; ok {
					item.Status = down
				}

				mutex.Unlock()
			}
			wg.Done()
		}(key, checker)
	}
	wg.Wait()

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(code)

	res := response{
		Status:  down,
		Errors:  errorMsgs,
		Details: h.details,
	}

	if code == http.StatusOK {
		res.Status = up
	}

	json.NewEncoder(w).Encode(res)
}

type timeoutChecker struct {
	checker Checker
}

func (t *timeoutChecker) Check(ctx context.Context) error {
	checkerChan := make(chan error)
	go func() {
		checkerChan <- t.checker.Check(ctx)
	}()
	select {
	case err := <-checkerChan:
		return err
	case <-ctx.Done():
		return errors.New("max check time exceeded")
	}
}

// WithTimeout configures the global timeout for all individual checkers.
func WithTimeout(timeout time.Duration) Option {
	return func(h *health) {
		h.timeout = timeout
	}
}
